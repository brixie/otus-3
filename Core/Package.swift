// swift-tools-version: 5.10

import PackageDescription

let package = Package(
    name: "Core",
    platforms: [
        .macOS(.v11),
        .iOS(.v14)
    ],
    products: [
        .library(name: "Core",
                 targets: ["Core"]),
    ], dependencies: [
        .package(url: "https://github.com/SDWebImage/SDWebImageSwiftUI.git", from: "3.0.0")
    ],
    targets: [
        .target(name: "Core",
                dependencies:  ["SDWebImageSwiftUI"])
    ]
)

//
//  BeanAPIService.swift
//
//
//  Created by Vladimir Gusev on 20.06.2024.
//

import Foundation

public final class BeanAPIService {
    
    public init() {}
}

extension BeanAPIService {
    
    public struct Parameters {
        public var pageIndex: Int = 0
        public var pageSize: Int = 10
        public var groupName: String? = nil
        public var flavorName: String? = nil
        public var colorGroup: String? = nil
        public var glutenFree: Bool? = nil
        public var sugarFree: Bool? = nil
        public var seasonal: Bool? = nil
        public var kosher: Bool? = nil
        
        public init(
            pageIndex: Int = 0,
            pageSize: Int = 10,
            groupName: String? = nil,
            flavorName: String? = nil,
            colorGroup: String? = nil,
            glutenFree: Bool? = nil,
            sugarFree: Bool? = nil,
            seasonal: Bool? = nil,
            kosher: Bool? = nil
        ) {
            self.pageIndex = pageIndex
            self.pageSize = pageSize
            self.groupName = groupName
            self.flavorName = flavorName
            self.colorGroup = colorGroup
            self.glutenFree = glutenFree
            self.sugarFree = sugarFree
            self.seasonal = seasonal
            self.kosher = kosher
        }
    }
    
    public func getBeans(parameters: Parameters,
                         completion: @escaping ((Result<BeansList, Error>) -> Void)) {
        DefaultAPI.getAllBeans(pageIndex: parameters.pageIndex,
                               pageSize: parameters.pageSize,
                               groupName: parameters.groupName,
                               flavorName: parameters.flavorName,
                               colorGroup: parameters.colorGroup,
                               glutenFree: parameters.glutenFree,
                               sugarFree: parameters.sugarFree,
                               seasonal: parameters.seasonal,
                               kosher: parameters.kosher) { data, error in
            if let error {
                completion(.failure(error))
                return
            }
            
            if let data {
                completion(.success(data))
                return
            }
            
            completion(.failure(ApiError.unexpectedBehavior))
        }
    }
    
    public enum ApiError: Error {
        case  unexpectedBehavior
    }
}

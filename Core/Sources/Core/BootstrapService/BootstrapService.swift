//
//  BootstrapService.swift
//
//
//  Created by Vladimir Gusev on 21.06.2024.
//

#if canImport(UIKit)
import UIKit
#endif

public final class BootstrapService {
    
    public init() {}
    
#if canImport(UIKit)
    public func setupApplication(with options: [UIApplication.LaunchOptionsKey: Any]?) {}
#endif
    
    public func registerServices(serviceLocator: ServiceLocator) {
        serviceLocator.register(BeanAPIService.self, factory: {
            return BeanAPIService()
        }())
        serviceLocator.register((any ImageDownloaderInterface).self, factory: {
            return ImageDownloader()
        }())
    }
}

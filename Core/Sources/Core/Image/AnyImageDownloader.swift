//
//  AnyImageDownloader.swift
//  
//
//  Created by Vladimir Gusev on 21.06.2024.
//

import Foundation
import Combine

#if canImport(UIKit)
import UIKit
#endif

public class AnyImageDownloader: ImageDownloaderInterface {

#if canImport(UIKit)
    public var image: UIImage? {
        get {
            return viewModel.image
        }
        set {
            viewModel.image = image
        }
    }
#endif
    
    private var cancellables = Set<AnyCancellable>()
    private let viewModel: any ImageDownloaderInterface
    
    init<ViewModel: ImageDownloaderInterface>(wrapping viewModel: ViewModel) {
        self.viewModel = viewModel
        
        viewModel.objectWillChange.sink { [weak self] _ in
            self?.objectWillChange.send()
        }.store(in: &cancellables)
    }
    
    public func load(url: URL?) {
        viewModel.load(url: url)
    }
    
    public func cancel() {
        viewModel.cancel()
    }
}

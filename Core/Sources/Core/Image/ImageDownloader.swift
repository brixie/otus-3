//
//  ImageDownloader.swift
//  
//
//  Created by Vladimir Gusev on 21.06.2024.
//

import Foundation
import Combine
import SDWebImageSwiftUI

#if canImport(UIKit)
import UIKit
#endif

final class ImageDownloader: ImageDownloaderInterface {
    
#if canImport(UIKit)
    var image: UIImage? {
        get {
            return manager.image
        }
        set {
            manager.image = image
        }
    }
#endif
    
    @Published private var manager = ImageManager()
    
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        manager.objectWillChange.sink { [weak self] _ in
            self?.objectWillChange.send()
        }.store(in: &cancellables)
    }
    
    func load(url: URL?) {
        manager.load(url: url)
    }
    
    func cancel() {
        manager.cancel()
    }
}

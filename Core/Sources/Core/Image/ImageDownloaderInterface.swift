//
//  ImageDownloaderInterface.swift
//
//
//  Created by Vladimir Gusev on 21.06.2024.
//

import Foundation

#if canImport(UIKit)
import UIKit
#endif

public protocol ImageDownloaderInterface: ObservableObject {
    
#if canImport(UIKit)
    var image: UIImage? { get set }
#endif
    
    func load(url: URL?)
    func cancel()
}

extension ImageDownloaderInterface {
    
    public func eraseToAnyImageDownloader() -> AnyImageDownloader {
        return AnyImageDownloader(wrapping: self)
    }
}

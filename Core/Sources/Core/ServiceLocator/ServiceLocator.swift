//
//  ServiceLocator.swift
//  
//
//  Created by Vladimir Gusev on 21.06.2024.
//

import Foundation

public final class ServiceLocator: ServiceLocatorInterface, ServiceLocatorRegistering {
    
    private var services: [AnyHashable: Any] = [:]
    
    private let queue = DispatchQueue(label: "ServiceLocator.Queue")
    
    public init() {}
    
    public func register<T>(_ type: T.Type, factory: @autoclosure () -> T) {
        queue.sync {
            let typeName = typeName(type)
            services[typeName] = factory()
        }
    }
    
    public func resolve<T>() -> T? {
        queue.sync {
            return services[typeName(T.self)] as? T
        }
    }
    
    public func resolve<T>(_ type: T.Type) -> T? {
        queue.sync {
            return services[typeName(type)] as? T
        }
    }
}

private extension ServiceLocator {
    
    func typeName<T>(_ type: T.Type) -> AnyHashable {
        return String(describing: type)
    }
}

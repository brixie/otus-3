//
//  ServiceLocatorInterface.swift
//
//
//  Created by Vladimir Gusev on 21.06.2024.
//

public protocol ServiceLocatorInterface {
    func resolve<T>(_ type: T.Type) -> T?
    func resolve<T>() -> T?
}

public protocol ServiceLocatorRegistering {
    func register<T>(_ type: T.Type, factory: @autoclosure () -> T)
}

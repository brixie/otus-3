//
//  AppDelegate.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 21.06.2024.
//

import UIKit
import Core

final class AppDelegate: NSObject, UIApplicationDelegate {
    
    let serviceLocator = ServiceLocator()
    
    private let bootstrapService = BootstrapService()
    
    override init() {
        super.init()
        
        bootstrapService.registerServices(serviceLocator: serviceLocator)
    }

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        bootstrapService.setupApplication(with: launchOptions)
        return true
    }
}

//
//  OtusApp.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 15.06.2024.
//

import SwiftUI

@main
struct OtusApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    private lazy var dataSource = DataSource(serviceLocator: appDelegate.serviceLocator)

    var body: some Scene {
        WindowGroup {
            ContentView(datasource: getDatasource())
        }
    }
}

private extension OtusApp {
    
    func getDatasource() -> DataSource {
        var mutableSelf = self
        return mutableSelf.dataSource
    }
}

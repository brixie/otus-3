//
//  AnchorKey.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 17.06.2024.
//

import SwiftUI

struct AnchorKey: PreferenceKey {
    typealias Value = Anchor<CGPoint>?
    static var defaultValue: Value { nil }
    
    static func reduce(value: inout Value, nextValue: () -> Value) {
        value = nextValue()
    }
}

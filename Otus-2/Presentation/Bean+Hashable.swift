//
//  Bean+Hashable.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 16.06.2024.
//

import Foundation
import Core

extension Bean {

    public func hash(into hasher: inout Hasher) {
        hasher.combine(beanId)
    }
}

//
//  ContentView.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 15.06.2024.
//

import SwiftUI
import Core

struct ContentView: View {
    
    @State private var selectedType: BeanType = .all
    
    @ObservedObject var datasource: DataSource
    
    var segmentControl: some View {
        Picker(selection: $selectedType) {
            Text("All").tag(BeanType.all)
            Text("Kosher").tag(BeanType.kosher)
            Text("Seasonal").tag(BeanType.seasonal)
            Text("Gluten Free").tag(BeanType.glutenFree)
            Text("Sugar Free").tag(BeanType.sugarFree)
        } label: {}
            .pickerStyle(SegmentedPickerStyle())
            .padding(.horizontal)
            .onChange(of: selectedType) { oldValue, newValue in
                guard oldValue != selectedType else { return }
                datasource.fetchNextPage(beanType: selectedType, shouldReset: true)
            }
    }
    
    struct ItemInfo {
        let bean: Bean
        let anchor: Anchor<CGPoint>
        let index: Int
    }
    
    @State private var selectedBeans = [ItemInfo]()
    
    @ViewBuilder
    func destination(bean: Bean?) -> some View {
        VStack {
            if let bean {
                Text("\(bean.flavorName)")
                
                NavigationLink {
                    VStack {
                        Text("Third screen")
                    }
                } label: {
                    Text("Go Deeper")
                }
            }
        }
    }
    
    @State private var navigate = false
    @State private var selectedItem: Bean?
    
    private let delay: Double = 2.0
    
    private func navigateToDetail(item: Bean) {
        selectedItem = item
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            navigate = true
        }
    }
    
    var lastRow: any View {
        switch datasource.state {
        case .loadingNextPage:
            ProgressView()
        case .data, .nextPageData:
            ProgressView()
                .onAppear {
                    datasource.fetchNextPage(beanType: selectedType)
                }
        case .idle:
            EmptyView()
        }
    }
    
    var contentList: some View {
        List {
            ForEach(datasource.beans.indices, id: \.self) { index in
                Button {
                    navigateToDetail(item: datasource.beans[index])
                } label: {
                    RowItem(index: index,
                            bean: datasource.beans[index],
                            imageDownloader: datasource.imageDownloader.eraseToAnyImageDownloader())
                        .frame(height: 78)
                        .anchorPreference(key: AnchorKey.self, value: .topLeading, transform: { $0 })
                }
                .overlayPreferenceValue(AnchorKey.self, { anchor in
                    Button {
                        selectedBeans.append(ItemInfo(bean: self.datasource.beans[index],
                                                      anchor: anchor!,
                                                      index: index))
                    } label: { Color.clear }
                })
                .listRowSeparator(.hidden)
                .listRowInsets(EdgeInsets(top: 4, leading: 12, bottom: 4, trailing: 12))
            }
            
            if datasource.hasMore {
                AnyView(lastRow)
                    .listRowSeparator(.hidden)
            }
        }
        .listStyle(.plain)
        .background(
            NavigationLink(destination:  destination(bean: selectedItem), isActive: $navigate) {
                EmptyView()
            }
        )
        .scrollContentBackground(.hidden)
        .overlay {
            if datasource.beans.isEmpty {
                
                if datasource.state != .loadingNextPage {
                    ContentUnavailableView("No Beans found for '\(selectedType.toString)'",
                                           systemImage: "magnifyingglass")
                } else if datasource.state == .loadingNextPage {
                    ProgressView()
                }
            }
        }
    }
    
    var targetView: some View {
        Color.clear
            .frame(height: 100)
    }
    
    var targetViews: some View {
        GeometryReader { proxy in
            ZStack {
                ForEach(Array(selectedBeans.enumerated()), id: \.offset) { (_, item) in
                    RowItem(index: item.index,
                            bean: item.bean,
                            imageDownloader: datasource.imageDownloader.eraseToAnyImageDownloader())
                        .frame(height: 100)
                        .offset(y: -300)
                        .padding(EdgeInsets(top: 4, leading: 12, bottom: 4, trailing: 12))
                        .animation(.easeInOut(duration: 1.0))
                        .transition(
                            .offset(x: proxy[item.anchor].x, y: proxy[item.anchor].y + 300)
                        )
                }
            }
        }
        .frame(height: 100)
    }
    
    var content: some View {
        ZStack {
            VStack {
                segmentControl
                contentList
            }
            
            VStack {
                targetView
                    .background(targetViews)
                Spacer()
            }
        }
    }
    
    var body: some View {
        NavigationStack {
            content
                .navigationTitle("Jelly Belly Wiki")
                .navigationDestination(for: Bean.self) { bean in
                    VStack {
                        Text("\(bean.flavorName)")
                    }
                }
        }
    }
}

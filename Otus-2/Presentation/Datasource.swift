//
//  Datasource.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 16.06.2024.
//

import Foundation
import Core

class DataSource: ObservableObject {
    @Published var beans: [Bean] = []
    
    var page = 1
    var pageSize = 20
    var hasMore = true
    
    enum State {
        case loadingNextPage
        case data
        case nextPageData
        case idle
    }
    
    var state: State = .data
    
    let imageDownloader: any ImageDownloaderInterface
    
    private let beanAPI: BeanAPIService

    init(serviceLocator: ServiceLocatorInterface) {
        self.beanAPI = serviceLocator.resolve()!
        self.imageDownloader = serviceLocator.resolve()!
    }
    
    func fetchNextPage(beanType: BeanType, shouldReset: Bool = false) -> Void {
        if shouldReset {
            reset()
        }
        
        guard
            state == .data || state == .nextPageData,
            hasMore
        else {
            return
        }
        
        state = .loadingNextPage
        
        var kosher: Bool?
        if beanType == .kosher {
            kosher = true
        }
        var glutenFree: Bool?
        if beanType == .glutenFree {
            glutenFree = true
        }
        var seasonal: Bool?
        if beanType == .seasonal {
            seasonal = true
        }
        var sugarFree: Bool?
        if beanType == .sugarFree {
            sugarFree = true
        }
        
        beanAPI.getBeans(
            parameters: .init(
                pageIndex: page,
                pageSize: pageSize,
                glutenFree: glutenFree,
                sugarFree: sugarFree,
                seasonal: seasonal,
                kosher: kosher
            )) { [weak self] result in
                guard let self else { return }
                switch result {
                case .success(let data):
                    beans.append(contentsOf: data.items)
                    hasMore = page <= data.totalPages
                    
                    if hasMore {
                        page += 1
                    }
                    
                    state = hasMore ? .nextPageData : .idle
                case .failure(let failure):
                    assertionFailure()
                }
            }
    }
    
    func reset() {
        beans.removeAll()
        hasMore = true
        page = 1
        state = .data
    }
}

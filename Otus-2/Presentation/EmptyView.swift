//
//  EmptyView.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 16.06.2024.
//

import SwiftUI

struct EmptyView: View {

    var body: some View {
        Color.clear
    }
}

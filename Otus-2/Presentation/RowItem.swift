//
//  RowItem.swift
//  Otus-2
//
//  Created by Vladimir Gusev on 17.06.2024.
//

import SwiftUI
import Core
import SDWebImageSwiftUI

struct RowItem<ImageDownloader: ImageDownloaderInterface>: View {
    
    let index: Int
    let bean: Bean
    @ObservedObject var imageDownloader: ImageDownloader
    
    var body: some View {
        HStack {
            Group {
                if imageDownloader.image != nil {
                    Image(uiImage: imageDownloader.image!)
                        .resizable()
                } else {
                    Rectangle().fill(Color.gray)
                }
            }
            .transition(.fade(duration: 0.5))
            .scaledToFit()
            .frame(width: 64, height: 64, alignment: .center)
            
            Text(bean.flavorName)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .onAppear {
            if let url = URL(string: bean.imageUrl) {
                imageDownloader.load(url: url)
            }
        }
        .onDisappear {
            imageDownloader.cancel()
        }
    }
}
